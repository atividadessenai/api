'use strict'

const userController = require('../Controllers/UserController.js')

class Auth {

  * handle(request, response, next) {

    if ((yield request.auth.check()) && request.currentUser.status !== 0) {

      const headers = request.headers()
      if (headers.latitude && headers.longitude && request.currentUser) {
        
        yield new userController().updateUserLastLocation(request.currentUser.id, headers.latitude, headers.longitude)
      }
      yield next
    } else {
      response.unauthorized({
        message: 'Usuário precisa estar logado para fazer essa ação'
      })
    }
  }

}

module.exports = Auth
