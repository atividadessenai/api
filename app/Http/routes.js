'use strict'

const Route = use('Route')


Route.group('authorization', function () {

  //user
  Route.put('users/power/:id', 'UserController.power') //-> admin DONE() T
  Route.delete('users/:id?', 'UserController.delete') // -> admin ou proprio usuario DONE()
  Route.get('/users/:id?', 'UserController.getUsers') // -> admin DONE()
  Route.put('users/:id?', 'UserController.update') // -> admin ou proprio usuario DONE()
  Route.put('users/activate/:id?', 'UserController.activate') // -> admin DONE()
  Route.put('users/notification/:id?', 'UserController.notification') // -> admin ou proprio DONE()
  Route.put('changePassword/', 'UserController.changePassword') // -> admin ou proprio DONE()

  //markup
  Route.post('markups/', 'MarkupController.add') //todos logados DONE()
  Route.put('markups/:id', 'MarkupController.update') // proprio usuario ou admin DONE()
  Route.delete('markups/:id?', 'MarkupController.delete') // proprio ou admin DONE()
  Route.get('/markuptypes/:id?', 'MarkupTypeController.getMarkupTypes') // -> admin

  //profile
  Route.get('/profiles/:id?', 'ProfileController.getProfiles') // -> admin DONE()
  Route.post('profiles/', 'ProfileController.add') // -> admin DONE()
  Route.put('profiles/:id?', 'ProfileController.update') // -> admin DONE()
  Route.delete('profiles/:id?', 'ProfileController.delete') // -> admin DONE()

  //image
  Route.get('/images?status=:status', 'ImageController.getNonApprovedImages') //-> moderador e admin DONE()
  Route.put('images/:id', 'ImageController.approveImage') //-> moderador e admin DONE()
  Route.get('/images/approved/', 'ImageController.getApprovedImages') // ->moderador e damin DONE()

  //notifications
  Route.post('notifications/', 'NotificationController.add') // -> insertNotification e admin DONE()
  Route.put('notifications/:id?', 'NotificationController.update') // -> proprio usuario ou admin DONE()
  Route.delete('notifications/:id?', 'NotificationController.delete') // -> proprio usuario ou admin DONE()

  //permission
  Route.get('/permissions/:id?', 'PermissionController.getPermissions') // -> admin
  Route.post('permissions/', 'PermissionController.add') // -> admin
  Route.put('permissions/:id?', 'PermissionController.update') // -> admin
  Route.delete('permissions/:id?', 'PermissionController.delete') // -> admin

  //profilehaspermission
  Route.get('/profilepermissions/:id?', 'ProfileHasPermissionController.getProfileRelated')

  Route.post('/vote', 'VoteController.add')
  Route.get('/vote/:id?', 'VoteController.userHasvoted')

}).middleware('authorization')

//notifications
Route.get('/notifications', 'NotificationController.getNotifications')
Route.get('/notifications/:id?', 'NotificationController.getNotifications')
//Route.get('/vote', 'VoteController.get')

//markups
Route.get('/markups', 'MarkupController.getMarkups')
Route.get('/markups/:id', 'MarkupController.getMarkupInfo')

Route.get('/markups/getUsersLocation', 'MarkupController.getUsersByLocation')

//user
Route.post('users/', 'UserController.add')
Route.post('login', 'UserController.doLogin')
Route.post('/password', 'UserController.resetPassword')

//Route.get('/logout', 'UserController.doLogout')

// Route.get('/markup', 'MarkupController.index')
// Route.get('/', 'UserController.welcome')
// Route.post('/test', 'MarkupController.test')
// Route.get('/register', 'UserController.viewRegister')
// Route.get('/login', 'UserController.viewLogin')
// Route.get('/home', 'UserController.viewLogin')

//profileHasPermission
// Route.get('/profilepermissions/:id?', 'ProfileHasPermissionController.getPermissionRelated')
// Route.post('profilepermissions/', 'ProfileHasPermissionController.add').as('addPermission')
// Route.delete('profilepermissions/:id?', 'ProfileHasPermissionController.delete')

//Route.get('/images/:id?', 'ImageController.getImages')
//Route.post('images/', 'ImageController.add')
//Route.delete('images/:id?', 'ImageController.delete')

//markupType
//Route.post('markuptypes/', 'MarkupTypeController.add')
//Route.put('markuptypes/:id?', 'MarkupTypeController.update')
//Route.delete('markuptypes/:id?', 'MarkupTypeController.delete')
