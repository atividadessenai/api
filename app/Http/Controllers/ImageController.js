'use strict'

const Image = use('App/Model/Image')
const Helpers = use('Helpers')
const Validator = use('Validator')
const AuthController = require('./AuthorizationController.js')

class ImageController {

  * add(request, response, markup_id) {
    const hasPermission = yield new AuthController().validPermissions(request, 'moderador');

    const imageObj = new Image();

    imageObj.markup_id = markup_id
    imageObj.user_id = request.currentUser.id
    imageObj.status = (request.currentUser && request.currentUser.is_admin == 1 || hasPermission) ?
      1 : request.input('status') || 0

    const image = request.file('image', {
      maxSize: '4mb',
      allowedExtensions: ['jpg', 'png', 'jpeg']
    })

    const fileName = `${new Date().getTime()}.${image.extension()}`
    yield image.move('public\\' + imageObj.markup_id, fileName)

    if (!image.moved()) {
      response.badRequest({
        message: 'Erro ao inserir imagem',
        object: image.errors()
      })
      return
    }

    imageObj.path = image.uploadPath()
    yield imageObj.save()

    yield response.ok({
      message: 'Imagem salva com sucesso',
      object: image
    })
  }

  * getImages(request, response) {
    const ret = request.param('id') !== null ? yield Image.findBy('id', request.param('id')) : yield Image.all();

    if (!ret)
      yield response.badRequest({
        message: '404 imagem não encontrada! :('
      })

    yield response.ok({
      message: 'Imagens obtidas com sucesso!',
      object: ret
    })
  }

  * getApprovedImages(request, response) {
    const hasPermission = yield new AuthController().validPermissions(request, 'moderador');

    if ((request.currentUser && request.currentUser.is_admin) || hasPermission) {

      const ret = yield Image.query().where('status', 1);

      if (!ret)
        yield response.badRequest({
          message: 'Não há Imagens aprovadas! :('
        })

      yield response.ok({
        message: 'Imagens obtidas com sucesso!',
        object: ret
      })
    } else {
      response.unauthorized({
        message: 'Usuário não tem essa permissão.'
      })
    }
  }

  * getNonApprovedImages(request, response) {
    const hasPermission = yield new AuthController().validPermissions(request, 'moderador');

    if ((request.currentUser && request.currentUser.is_admin) || hasPermission) {

      const ret = yield Image.query().where('status', 0);

      if (!ret)
        yield response.badRequest({
          message: 'Não há Imagens para aprovar! :)'
        })

      yield response.ok({
        message: 'Imagens obtidas com sucesso!',
        object: ret
      })
    } else {
      response.unauthorized({
        message: 'Usuário não tem essa permissão.'
      })
    }
  }

  * approveImage(request, response) {
    const hasPermission = yield new AuthController().validPermissions(request, 'moderador');

    if ((request.currentUser && request.currentUser.is_admin) || hasPermission) {

      const ret = request.param('id') !== null ? yield Image.findBy('id', request.param('id')) :
        yield response.badRequest({
          message: 'Imagem não encontrada!'
        });

      ret.status = 1;

      yield ret.save();

      yield response.ok({
        message: 'Imagem aprovada com sucesso!'
      })
    } else {
      response.unauthorized({
        message: 'Usuário não tem essa permissão.'
      })
    }
  }

  * delete(request, response) {
    const ret = request.param('id') === null ? yield response.badRequest({
        message: 'Sem id para procurar a imagem!'
      }) :
      yield Image.findBy('id', request.param('id'));

    ret === null ? yield response.badRequest({
        message: 'Imagem não encontrada'
      }) :
      yield ret.delete();

    yield response.ok({
      message: 'Imagem deletada!'
    })
  }

  * deleteByMarkupId(id) {
    const image = yield Image.findBy('markup_id', id)

    if (!image === null) {
      yield image.delete();
    }
  }

}

module.exports = ImageController
