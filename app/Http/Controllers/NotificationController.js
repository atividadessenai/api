'use strict'

const Notification = use('App/Model/Notification')
const Markup = use('App/Model/Markup')
const Database = use('Database')
const User = use('App/Model/User')
const Validator = use('Validator')
const AuthController = require('./AuthorizationController')
const Ws = use('Ws')

class NotificationController {

  * add(request, response) {

    const hasPermission = yield new AuthController().validPermission(request, 'notification')

    if (request.currentUser.is_admin == 1 || hasPermission) {

      const validation = yield Validator.validateAll(request.all(), Notification.rules)

      if (validation.fails()) {
        yield response.badRequest({
          message: validation.messages()
        })
      }

      const notification = new Notification()

      notification.user_id = request.currentUser.id
      notification.markup_id = request.input('markup_id')
      notification.description = request.input('description')
      notification.detail = request.input('detail')
      notification.sent = request.input('sent') || 0
      notification.status = 1

      yield notification.save()

      yield this.sendNotification('Nova notificação', notification)

      yield response.ok({
        message: 'Notificação salva com sucesso',
        object: notification
      })
    } else {
      response.unauthorized({
        message: 'Usuário não tem essa permissão'
      })
    }
  }

  * getNotifications(request, response) {

    const params = request.get(),
      page = params.page || 1,
      max = params.max || 10,
      id = (request.param('id') !== null);
    let ret;


    if (!id) {

      ret = yield Database.from('notifications').where('status', 1).paginate(page, max)
    } else {

      ret = yield Database.table('notifications').where('id', id).where('status', 1)
      ret = ret[0]

      if (ret.markup_id) {
        const query =
          `SELECT path
         FROM images
         WHERE markup_id = ${ret.markup_id} AND status = 1`;

        ret.path = (yield Database.raw(query)).shift()
      }
    }

    if (!ret)
      yield response.badRequest({
        message: 'Nenhuma notificação encontrada.'
    })

    yield response.ok({
      message: 'Notificações obtidas com sucesso',
      object: ret
    })
  }

  * update(request, response) {
    const notification = yield Notification.findBy('id', request.param('id'))

    if (request.currentUser.is_admin == 1 || notification.user_id == request.currentUser.id) {

      if (!notification)
        yield response.badRequest({
          message: 'Notificação inexistente'
        })

      const validation = yield Validator.validateAll(request.all(), Notification.editRules())

      if (validation.fails()) {
        yield response.badRequest({
          message: 'Informações preenchidas incorretamente!'
        })
      }

      notification.description = request.input('description') || notification.description
      notification.detail = request.input('detail') || notification.detail
      notification.sent = request.input('sent') || notification.detail

      yield notification.save()

      yield response.ok({
        message: 'Notificação atualizada com sucesso',
        object: notification
      })
    } else {
      response.unauthorized({
        message: 'Usuário não tem essa permissão.'
      })
    }

  }

  * delete(request, response) {
    const notification = yield Notification.findBy('id', request.param('id'))

    if (request.currentUser.is_admin == 1 || notification.user_id == request.currentUser.id) {

      if (!notification)
        yield response.badRequest({
          message: 'Notificação não encontrada'
        })

      notification.status = 0

      yield notification.save()

      yield response.ok({
        message: 'Notificação deletada com sucesso',
        object: notification.id
      })
    } else {
      response.unauthorized({
        message: 'Usuário não tem essa permissão.'
      })
    }
  }

  * sendNotification(notification, message, usersToSend) {
    if (Ws.io.engine.clientsCount > 0) {
      Ws.channel('socket').emit('newAlert', {
        message,
        notification: notification
      })
    }
  }

}

module.exports = NotificationController
