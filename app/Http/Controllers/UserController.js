'use strict'

const User = use('App/Model/User')
const Profile = use('App/Model/Profile')
const Hash = use('Hash')
const Database = use('Database')
const Validator = use('Validator')
const Email = require('../../Helpers/SendMail.js')

class UserController {

  //Load Views
  * viewRegister(request, response) {
    yield response.sendView('register')
  }

  * viewLogin(request, response) {
    yield response.sendView('login')
  }

  * welcome(request, response) {
    yield response.sendView('welcome')
  }

  //End of Load Views

  * getUsers(request, response) {

    if (request.currentUser.is_admin == 1) {
      let query = `SELECT u.id AS user_id , pr.id AS profile_id, u.name, 
    u.email, u.receive_notification, pr.description AS profile_description, 
    group_concat(pe.description) AS permissions 
    FROM users u
    INNER JOIN profiles pr ON pr.id = u.profile_id AND pr.status = 1
    LEFT JOIN profile_has_permissions php ON  php.profile_id = pr.id
    LEFT JOIN permissions pe ON pe.id = php.permission_id
    WHERE u.status = 1 `


      if (request.param('id')) {
        query += `AND u.id =` + request.param('id')
      }

      query += ` GROUP BY u.id`

      let ret = (yield Database.raw(query)).shift()

      if (!ret)
        yield response.ok({
          message: 'Usuários não encontrados.'
        })

      yield response.ok({
        message: 'Operação feita com sucesso!',
        object: ret
      })
    } else {
      response.unauthorized({
        message: 'Usuário não tem essa permissão.'
      })
    }
  }

  * add(request, response) {
    const user = new User()

    const validation = yield Validator.validateAll(request.all(), User.rules)

    if (validation.fails()) {
      yield response.badRequest({
        message: 'Informações preenchidas incorretamentes.'
      })
    }

    let ret = yield Profile.findBy('id', request.input('profile'))

    ret ? user.profile_id = request.input('profile') : yield response.badRequest({
      message: "Perfil inexistente"
    })

    user.name = request.input('name')
    user.email = request.input('email')
    user.status = request.input('status') || 1
    user.receive_notification = 1
    user.password = request.input('password')
    user.is_admin = request.currentUser && request.currentUser.is_admin &&
      request.input('is_admin') ? request.input('is_admin') : 0

    yield user.save()
    user.password = ""

    yield response.ok({
      message: 'Operação feita com sucesso!',
      object: user
    })

  }

  * delete(request, response) {
    if (request.currentUser.is_admin == 1 || request.currentUser.id === request.param('id')) {
      const user = yield User.findBy('id', request.param('id'));
      if (!user) {
        yield response.ok({
          message: 'Usuário não encontrado.',
        });
      }

      user.status = 0

      yield user.save()

      yield response.ok({
        message: 'Operação feita com sucesso!',
        object: user.id
      })
    } else {
      response.unauthorized({
        message: 'Usuário nâo tem essa permissão.'
      })
    }
  }

  * notification(request, response) {
    if (request.currentUser.is_admin == 1 || request.currentUser.id === request.param('id')) {
      const user = yield User.findBy('id', request.param('id'));
      if (!user) {
        yield response.ok({
          message: 'Nenhuma notificação encontrada.',
        });
      }

      user.receive_notification = user.receive_notification === 1 ? 0 : 1;

      yield user.save()

      yield response.ok({
        message: user.receive_notification === 1 ? 'Notificações ativadas' : 'Notificações desativadas',
        object: user.id
      })
    } else {
      response.unauthorized({
        message: 'Usuário não tem essa permissão.'
      })
    }
  }

  * activate(request, response) {

    if (request.currentUser.is_admin == 1) {

      const user = yield User.findBy('id', request.param('id'));
      if (!user) {
        yield response.ok({
          message: 'Usuário não encontrado.',
        });
      }

      user.status = user.status == 1 ? 0 : 1

      yield user.save()

      yield response.ok({
        message: `Usuario ${user.status ? 'ativado.' : 'desativado.'}`,
        object: user.id
      })
    } else {

      response.unauthorized({
        message: 'Usuário não tem essa permissão.'
      })
    }
  }

  * update(request, response) {
    if (request.currentUser.is_admin == 1 || request.currentUser.id === request.param('id')) {
      let user = yield User.findBy('id', request.param('id'))

      if (!user) yield response.badRequest({
        message: "Usuário não encontrado."
      })

      const profile = yield Profile.findBy('id', request.input('profile'))

      user.name = request.input('name') || user.name
      user.receive_notification = request.input('receive_notification') === 1 ? 1 : 0
      user.profile_id = !profile ? user.profile_id : profile.id

      yield user.save()

      user.password = "encrypted"

      yield response.ok({
        message: 'Operação feita com sucesso!',
        object: user
      })
    } else {
      response.unauthorized({
        message: 'Usuário não tem essa permissão.'
      })
    }
  }

  * changePassword(request, response) {

    try {
      
      yield request.auth.validate(request.currentUser.email, request.input('password'))

      const user = yield User.findBy('id', request.currentUser.id)

      const password = yield Hash.make(request.input('newPassword'))
      user.password = password

      yield user.save()

      response.ok({
        message: 'Senha alterada com sucesso.'
      })

    } catch (e) {

      response.badRequest({
        message: 'Senha digitada não confere com a atual.'
      })
    }

  }

  * doLogin(request, response) {

    const isLoggedIn = yield request.auth.check()

    if (!isLoggedIn || request.currentUser.status === 0) {
      yield response.unauthorized({
        message: "Usuario e/ou senha incorretos ou o usuário está desativado."
      });
    }

    let user = yield User.findBy('id', request.currentUser.id);
    user.profile = yield Profile.findBy('id', request.currentUser.profile_id);
    user.password = 'encrypted'

    return yield response.ok({
      message: 'Usuário autenticado',
      object: user.attributes
    })
  }

  * doLogout(request, response) {
    yield request.auth.logout()

    return response.ok('Usuário deslogado com sucesso')
  }

  * power(request, response) {
    if (request.currentUser.is_admin == 1) {
      const user = yield User.findBy('id', request.param('id'));

      if (!user) {
        response.badRequest({
          message: 'Usuário não encontrado'
        })
      }

      user.is_admin = user.is_admin == 1 ? 0 : 1

      yield user.save()

      response.ok({
        message: `Alterado para ${user.is_admin == 1 ? 'admin': 'usuário convencional'}!`,
        object: user.id
      })
    } else {
      response.unauthorized({
        message: 'Usuário não tem essa permissão.'
      })
    }

  }

  * updateUserLastLocation(userId, lat, long) {
    const user = yield User.findBy('id', userId);

    user.last_latitude = lat;
    user.last_longitude = long;

    yield user.save()
  }

  * resetPassword(request, response) {
    const user = yield User.findBy('email', request.input('user_email'))

    if (!user) {
      response.badRequest({
        message: 'E-mail não consta na nossa base de dados.'
      })
    }

    const password = yield this.generateNewPassword()

    yield new Email().mail(user, password)

    user.password = yield Hash.make(password)

    yield user.save()

    response.ok({
      message: 'Senha redefinida e E-mail enviado com sucesso.'
    })
  }

  * generateNewPassword() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%&*()_-+=";

    for (var i = 0; i < 10; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text
  }

}

module.exports = UserController
