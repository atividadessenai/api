'use strict'

const ProfileHasPermission = use('App/Model/Profile_Has_Permission')
const Profile = use('App/Model/Profile')
const Permission = use('App/Model/Permission')
const Validator = use('Validator')
const Database = use('Database')

class ProfileHasPermissionController {

  * add(response) {

    // const validation = yield Validator.validateAll(request.all(), ProfileHasPermission.rules)

    // if (validation.fails()) {
    //   yield response.unauthorized({
    //     message: 'Informações preenchidas incorretamente!'
    //   })
    // }

    // if (!Profile.find(request.input('profile_id')) && !Permission.find(request.input('permission_id')))
    //   yield response.unauthorized({
    //     message: 'Objeto não encontrado!'
    //   })

    // const profilehaspermission = new ProfileHasPermission()

    // profilehaspermission.profile_id = response.id
    // profilehaspermission.permission_id = request.input('permission_id')

    // yield profilehaspermission.save()

    yield response.ok({
      message: 'Perfil relacionado com permissões',
      object: response
    })

  }

  * getProfileRelated(request, response) {

    if (request.currentUser.is_admin !== 1) {
      response.unauthorized({
        message: "Usuário não tem essa permissão."
      })
    }

    const ret = (request.param('id') !== null) ? yield ProfileHasPermission.query()
      .select('profiles.id as idprofile', 'permissions.id as idpermission')
      .innerJoin('permissions', 'profile_has_permissions.permission_id', 'permissions.id')
      .innerJoin('profiles', 'profile_has_permissions.profile_id', 'profiles.id')
      .where('profile_has_permissions.profile_id', request.param('id')).fetch() :
      yield ProfileHasPermission.all();

    if (!ret)
      yield response.badRequest({
        message: 'Nenhum relacionamento encontrado entre perfil e permissão.'
      })

    yield response.ok({
      message: 'Relacionamentos de perfil com permissão obtidos com sucesso',
      object: ret
    })
  }

  * getPermissionRelated(request, response) {

    if (request.currentUser.is_admin !== 1) {
      response.unauthorized({
        message: "Usuário não tem essa permissão."
      })
    }

    const ret = (request.param('id') !== null) ?
      yield ProfileHasPermission.query()
      .innerJoin('profiles', 'profile_has_permissions.profile_id', 'profiles.id')
      .innerJoin('permissions', 'profile_has_permissions.permission_id', 'permissions.id')
      .where('profile_has_permissions.permission_id', request.param('id')).fetch() :
      yield ProfileHasPermission.all();

    if (!ret)
      yield response.badRequest({
        message: 'Nenhuma permissão relacionada encontrada.'
      })

    yield response.ok({
      message: 'Relacionamentos de permissão com perfil obtidos com sucesso',
      object: ret
    })
  }

  * delete(request, response) {
    const ProfileHasPermission = yield ProfileHasPermission.findByOrFail('profile_id', request.param('id'))

      !ProfileHasPermission ?
      yield response.badRequest({
        message: 'Relacionamento não encontrado.'
      }) :
      yield ProfileHasPermission.delete();

    yield response.ok({
      message: 'Relacionamentos de perfil e permissão deletado com sucesso',
      object: ProfileHasPermission.id
    })
  }

  * deleteByProfileOrPermissionId(id, isProfile) {

    return (yield Database.raw(`DELETE FROM profile_has_permissions WHERE ${isProfile ? 'profile_id' : 'permission_id'} = ${id}`))
  }
}

module.exports = ProfileHasPermissionController
