'use strict'

const Markup = use('App/Model/Markup')
const User = use('App/Model/User')
const Image = use('App/Model/Image')
const Notification = use('App/Model/Notification')
const NotificationController = require('./NotificationController.js')
const Markup_Type = use('App/Model/Markup_Type')
const ImageController = require('./ImageController.js')
const Helpers = use('Helpers')
const Validator = use('Validator')
const Database = use('Database')

class MarkupController {

  * index(request, response) {
    yield response.sendView('markup')
  }

  * add(request, response) {

    const validation = yield Validator.validateAll(request.all(), Markup.rules)

    if (validation.fails()) {
      yield response.badRequest({
        message: validation.messages()
      })
    }

    const markup = new Markup()

    const trx = yield Database.beginTransaction()

    markup.user_id = request.currentUser.id

    const ret = yield Markup_Type.findBy('id', request.input('markup_type_id'))

    if (ret) {
      markup.markup_type_id = request.input('markup_type_id')
    } else {
      trx.rollback()
      yield response.badRequest({
        message: "Tipo de marcação não encontrada!"
      })
    }

    markup.latitude = request.input('latitude')
    markup.longitude = request.input('longitude')
    markup.description = request.input('description')
    markup.status = request.currentUser.profile_id === 3 ? request.input('status') || 1 : 1
    markup.useTransaction(trx)

    yield markup.save()

    if (request.currentUser.profile_id === 3 && request.input('createNotification') === 1) {

      if (request.input('n_description') || request.input('n_detail') || markup.id) {
        trx.rollback()
        yield response.badRequest({
          message: 'Erro ao salvar notificação!'
        })
      }
      const notification = new Notification()

      notification.description = request.input('n_description')
      notification.detail = request.input('n_detail')
      notification.markup_id = markup.id
      notification.user_id = request.currentUser.id
      notification.status = request.input('n_status') || 1
      notification.useTransaction(trx)

      yield markup.notification().save(notification)
    }

    if (request.input('image')) {
      let imageController = new ImageController();
      markup.image = yield imageController.add(request, response, markup.id)
    }

    trx.commit()

    if (request.currentUser.is_admin === 1) {
      yield this.warn()
    }

    yield response.ok({
      message: 'Operação feita com sucesso!',
      object: markup
    })
  }

  * warn() {

    yield new NotificationController().sendNotification('CUIDADO! Nova marcação perto de você.', null, null)

  }

  * getUsersByLocation(lat, long) {

    if (lat && long) {
      const sql =
        `SELECT id, (6371 * acos(
        cos(radians(${lat})) *
        cos(radians(users.last_latitude)) *
        cos(radians(${long}) - radians(users.last_longitude)) +
        sin(radians(${lat})) *
        sin(radians(users.last_latitude)) 
      )) AS distance 
      FROM users
      WHERE users.receive_notification = 1 AND users.status = 1
      HAVING distance <= 2`

      return (yield Database.raw(sql)).shift()

    }

  }

  * getMarkupInfo(request, response) {

    if (request.param('id') === null || isNaN(request.param('id'))) {
      yield response.badRequest({
        message: 'Id não informado'
      })
    }

    const query =
      `SELECT m.id, m.user_id, m.latitude, m.longitude, 
      m.description, m.markup_type_id, m.created_at, m.updated_at, i.path
      FROM markups m
      LEFT JOIN images i ON i.markup_id = m.id AND i.status = 1
      WHERE m.id = ${request.param('id')}`;

    const ret = (yield Database.raw(query)).shift()

    if (!ret)
      yield response.badRequest({
        message: 'Nenhuma marcação encontrada'
      })

    yield response.ok({
      message: 'Operação feita com sucesso!',
      object: ret[0]
    })
  }

  * getMarkups(request, response) {

    const params = request.get();
    let having = '',
      query = `SELECT id, latitude, longitude, markup_type_id, 
				description, CAST(created_at AS DATETIME) 
				as created_date, status`;

    if (params.lat && params.long) {
      query += `, (6371 * acos(
					cos(radians(${params.lat})) *
					cos(radians(markups.latitude)) *
					cos(radians(${params.long}) - radians(markups.longitude)) +
					sin(radians(${params.lat})) *
					sin(radians(markups.latitude)) 
				)) AS distance `;
      having = ` HAVING distance <= ${params.distance || '5'}`;
    }

    query += ` FROM markups WHERE status= ${params.filter ? yield this.setStatus(params.filter) : '1'}`;

    if (params.type) {

      query += ' AND markup_type_id =' + params.type
    }

    if (params.initialDate && params.endDate) {

      const initialDate = params.initialDate.split('-')[2] + '-' + params.initialDate.split('-')[1] + '-' + params.initialDate.split('-')[0]
      const endDate = params.endDate.split('-')[2] + '-' + params.endDate.split('-')[1] + '-' + params.endDate.split('-')[0]
      query += ' AND created_at BETWEEN "' + initialDate + '" AND "' + endDate + '"'
    }

    query += having;

    const ret = (yield Database.raw(query)).shift()

    if (!ret)
      yield response.badRequest({
        message: 'Nenhuma marcação encontrada.'
      })

    yield response.ok({
      message: 'Operação feita com sucesso!',
      object: ret
    })
  }

  * update(request, response) {
    let markup = yield Markup.findBy('id', request.param('id'))
    if (request.currentUser.is_admin == 1 || request.currentUser.id === markup.user_id) {
      if (!markup) yield response.badRequest({
        message: 'Marcação não encontrada.'
      })

      const validation = yield Validator.validateAll(request.all(), Markup.editRules())

      if (validation.fails()) {
        yield response.badRequest({
          message: 'Informações preenchidas incorretamente!'
        })
      }

      markup.description = request.input('description') || markup.description

      yield markup.save()

      yield response.ok({
        message: 'Operação feita com sucesso!',
        object: markup
      })
    } else {
      response.unauthorized({
        message: 'Usuário não tem essa permissão.'
      })
    }
  }

  * delete(request, response) {
    const markup = yield Markup.findBy('id', request.param('id'))
    if (request.currentUser.is_admin == 1 || request.currentUser.id === markup.user_id) {
      if (!markup) {
        yield response.badRequest({
          message: 'Marcação não encontrada!',
        });
      }

      markup.status = 0

      yield markup.save()

      let imageController = new ImageController()

      yield imageController.deleteByMarkupId(markup.id)

      yield response.ok({
        message: 'Marcação deletada com sucesso!',
        object: markup.id
      })
    } else {
      response.unauthorized({
        message: 'Usuário não tem essa permissão.'
      })
    }
  }

  * adminDelete(markup_id) {

    const markup = yield Markup.findBy('id', markup_id)

    markup.status = 0

    yield markup.save()

    let imageController = new ImageController()

    yield imageController.deleteByMarkupId(markup_id)
  }

  * setStatus(status) {
    switch (status) {
      case 'approved':
        return '1';
        break;
      case 'disapproved':
        return '2';
        break;
      case 'inapproval':
        return '0';
        break;
      default:
        return '1';
        break;
    }
  }

}

module.exports = MarkupController
