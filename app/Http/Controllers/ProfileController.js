	'use strict'

	const Profile = use('App/Model/Profile')
	const User = use('App/Model/User')
	const Permission = use('App/Model/Permission')
	const Validator = use('Validator')
	const PHP = use('App/Model/Profile_Has_Permission')
	const PHPController = require('./ProfileHasPermissionController')
	const Database = use('Database')

	class ProfileController {

	  * add(request, response) {
	    if (request.currentUser && request.currentUser.is_admin == 1) {

	      const validation = yield Validator.validateAll(request.all(), Profile.rules)

	      if (validation.fails()) {
	        yield response.badRequest({
	          message: 'Perfil já existente.'
	        })
	      }

	      const trx = yield Database.beginTransaction()

	      const profile = new Profile()
	      profile.description = request.input('description')
	      profile.status = 1
	      profile.useTransaction(trx)

	      yield profile.save();

	      let permissions = request.input('permissions')
	      if (permissions) {

	        for (let i = 0; i < permissions.length; i++) {

	          let ret = yield Permission.findBy('id', permissions[i])
	          if (!ret) {
	            trx.rollback();
	            yield response.badRequest({
	              message: 'Permissão inexistente'
	            })
	          }

	          let php = new PHP()
	          php.profile_id = profile.id
	          php.permission_id = permissions[i]
	          php.useTransaction(trx)
	          yield profile.php().save(php)
	        }
	      }
	      trx.commit()

	      yield response.ok({
	        message: 'Perfis salvos com sucesso',
	        object: profile
	      })
	    } else {
	      response.unauthorized({
	        message: 'Usuário não tem essa permissão.'
	      })
	    }
	  }

	  * update(request, response) {
	    if (request.currentUser.is_admin == 1) {
	      const profile = yield Profile.findBy('id', request.param('id'))

	      if (!profile) {
	        yield response.badRequest({
	          message: 'Perfil não encontrado.'
	        })
	      }

	      const validation = yield Validator.validateAll(request.all(), Profile.rules)

	      if (validation.fails()) {
	        yield response.badRequest({
	          message: 'Informações preenchidas incorretamente!'
	        })
	      }

	      const trx = yield Database.beginTransaction()

	      profile.description = request.input('description')
	      profile.useTransaction(trx)
	      let permissions = request.input('permissions')

	      if (permissions.length >= 0) {

	        let phpController = new PHPController()

	        yield phpController.deleteByProfileOrPermissionId(request.param('id'), true);

	        for (var i = 0; i < permissions.length; i++) {

	          let ret = yield Permission.findBy('id', permissions[i])
	          if (!ret) {
	            trx.rollback();
	            yield response.badRequest({
	              message: 'Permissão inexistente'
	            })
	          }

	          let php = new PHP()
	          php.profile_id = profile.id
	          php.useTransaction(trx)
	          php.permission_id = permissions[i]
	          yield profile.php().save(php)
	        }
	      }

	      yield profile.save()

	      trx.commit()

	      yield response.ok({
	        message: 'Perfil atualizado com sucesso',
	        object: profile
	      })
	    } else {
	      response.unauthorized({
	        message: 'Usuário não tem essa permissão.'
	      })
	    }
	  }

	  * getProfiles(request, response) {
	    if (request.currentUser.is_admin == 1) {
	      let query,
	        profile,
	        profile_id = request.param('id');
	      if (!profile_id) {
	        profile = yield Database.select('id', 'description').from('profiles').where('status', 1)

	        for (let i = 0; i < profile.length; i++) {
	          query = `SELECT pe.id AS id, pe.description AS description 
				  FROM permissions pe 
				  INNER JOIN profile_has_permissions php ON php.permission_id = pe.id 
				  WHERE php.profile_id = ` + profile[i].id;

	          profile[i].permissions = (yield Database.raw(query)).shift()
	        }

	      } else {

	        profile = yield Profile.findBy('id', profile_id);

	        query =
	          `SELECT pe.id AS id, pe.description AS description 
		FROM permissions pe 
		INNER JOIN profile_has_permissions php ON php.permission_id = pe.id
		WHERE php.profile_id =` + profile_id

	        profile.permissions = (yield Database.raw(query)).shift()
	      }

	      if (!profile) {
	        yield response.badRequest({
	          message: 'Perfil não encontrado.'
	        })
	      }

	      yield response.ok({
	        message: 'Perfis obtidos com sucesso',
	        object: profile_id ? profile.attributes : profile
	      })
	    } else {
	      response.unauthorized({
	        message: 'Usuário não tem essa permissão.'
	      })
	    }
	  }

	  * delete(request, response) {
	    if (request.currentUser.is_admin == 1) {
	      const profile = yield Profile.findBy('id', request.param('id'))

	      if (!profile) {
	        yield response.badRequest({
	          message: 'Perfil não existente'
	        })
				}
				
				const hasUsers = yield User.findBy('profile_id', profile.id)

				if(hasUsers){
					yield response.badRequest({
	          message: 'Perfil não pode ser excluído pois há usuários vinculados.'
	        })
				}

	      profile.status = 0

	      yield profile.save()

	      let php = new PHPController()

	      yield php.deleteByProfileOrPermissionId(profile.id, true);

	      yield response.ok({
	        message: 'Perfil deletado com sucesso',
	        object: profile.id
	      })
	    } else {
	      response.unauthorized({
	        message: 'Usuário não tem essa permissão.'
	      })
	    }
	  }

	}

	module.exports = ProfileController
