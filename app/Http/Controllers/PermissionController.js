'use strict'

const Permission = use('App/Model/Permission')
const Validator = use('Validator')
const PHPController = require('./ProfileHasPermissionController')

class PermissionController {

  * add(request, response) {

    if (request.currentUser.is_admin !== 1) {
      response.unauthorized({
        message: "Usuário não tem essa permissão."
      })
    }

    const validation = yield Validator.validateAll(request.all(), Permission.rules)

    if (validation.fails()) {
      yield response.badRequest({
        message: 'Informações preenchidas incorretamente!'
      })
    }

    const permission = new Permission()

    permission.slug = request.input('slug')
    permission.description = request.input('description')

    yield permission.save()

    yield response.ok({
      message: 'Permissão salva com sucesso',
      object: permission
    })
  }

  * getPermissions(request, response) {

    if (request.currentUser.is_admin !== 1) {
      response.unauthorized({
        message: "Usuário não tem essa permissão."
      })
    }

    const ret = (request.param('id') !== null) ? yield Permission.findBy('id', request.param('id')) : yield Permission.all();

    if (!ret) yield response.badRequest({
      message: 'Permissão não encontrada'
    })

    yield response.ok({
      message: 'Permissões obtidas com sucesso',
      object: ret
    })
  }

  * update(request, response) {

    if (request.currentUser.is_admin !== 1) {
      response.unauthorized({
        message: "Usuário não tem essa permissão."
      })
    }

    const permission = yield Permission.findBy('id', request.param('id'))

    if (!permission) yield response.badRequest({
      message: 'Permissão não encontrada.'
    })

    const validation = yield Validator.validateAll(request.all(), Permission.editRules)

    if (validation.fails()) {
      yield response.badRequest({
        message: 'Informações preenchidas incorretamente!'
      })
    }

    permission.description = request.input('description')

    yield permission.save()

    yield response.ok({
      message: 'Permissão atualizada com sucesso',
      object: permission
    })

  }

  * delete(request, response) {

    if (request.currentUser.is_admin !== 1) {
      response.unauthorized({
        message: "Usuário não tem essa permissão."
      })
    }

    const permission = yield Permission.findBy('id', request.param('id'))

    if (!permission)
      yield response.badRequest({
        message: 'Permissão não encontrada.'
      });

    yield permission.delete()

    let php = new PHPController()

    yield php.deleteByProfileOrPermissionId(permission.id, false);

    yield response.ok({
      message: 'Permissão deletada com sucesso',
      object: permission.id
    })
  }

}

module.exports = PermissionController
