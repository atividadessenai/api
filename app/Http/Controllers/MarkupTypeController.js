'use strict'

const MarkupType = use('App/Model/Markup_Type')
const Validator = use('Validator')

class MarkupTypeController {

  * add(request, response) {
    const validation = yield Validator.validateAll(request.all(), MarkupType.rules)

    if (validation.fails()) {
      yield response.badRequest({
        message: 'Informações preenchidas incorretamente!'
      })
    }

    const markuptype = new MarkupType()

    markuptype.duration = request.input('duration')
    markuptype.description = request.input('description')
    markuptype.status = 1

    yield markuptype.save()

    yield response.ok({
      message: 'Operação feita com sucesso!',
      object: markuptype
    })

  }

  * getMarkupTypes(request, response) {
    const ret = (request.param('id') !== null) ? yield MarkupType.findBy('id', request.param('id')) : yield MarkupType.all();

    if (!ret)
      yield response.badRequest({
        message: 'Tipo de marcação não encontrado.'
      })

    yield response.ok({
      message: 'Operação feita com sucesso!',
      object: ret
    })
  }

  * update(request, response) {
    const markuptype = yield MarkupType.findBy('id', request.param('id'))

    if (!markuptype)
      yield response.badRequest({
        message: 'Tipo de marcação não encontrada!'
      })

    markuptype.duration = request.input('duration') || markuptype.duration
    markuptype.description = request.input('description') || markuptype.description

    yield markuptype.save()

    yield response.ok({
      message: 'Operação feita com sucesso!',
      object: markuptype
    })
  }

  * delete(request, response) {
    const markuptype = yield MarkupType.findBy('id', request.param('id'))

    if (!markuptype)
      yield response.badRequest({
        message: 'Tipo de marcação não encontrada!'
      })

    markuptype.status = 0

    yield markuptype.save()

    yield response.ok({
      message: 'Marcação deletada com sucesso!',
      object: markuptype.id
    })
  }

}

module.exports = MarkupTypeController
