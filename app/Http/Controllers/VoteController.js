'use strict'

const Vote = use('App/Model/Vote')
const Markup = use('App/Model/Markup')
const MarkupController = require('./MarkupController.js')
const Database = use('Database')
const Validator = use('Validator')

class VoteController {

  * add(request, response) {

    const vote = new Vote()

    const validation = yield Validator.validateAll(request.all(), Vote.rules)

    if (validation.fails()) {
      yield response.badRequest({
        message: 'Informações preenchidas incorretamentes.'
      })
    }

    const markup_id = request.input('markup_id')

    const ret = yield Markup.findBy('id', markup_id)

    ret ? vote.markup_id = markup_id : yield response.badRequest({
      message: "Marcação inexistente"
    })

    const isValid = yield this.validVote(markup_id, request.currentUser.id)

    if (isValid) {
      yield response.badRequest({
        message: "Esse usuário já votou para essa marcação."
      })
    }

    vote.user_id = request.currentUser.id
    vote.type = request.input('type')

    yield vote.save()

    const count = yield this.delete(markup_id)

    if (request.currentUser.is_admin === 1 && vote.type == 2 || vote.type == 2 && count.ids > 10) {
      yield new MarkupController().adminDelete(markup_id)
    }

    yield response.ok({
      message: 'Operação feita com sucesso!',
      object: vote
    })
  }

  * delete(markup_id) {
    const count = yield Database.from('votes').where('markup_id', markup_id)
      .where('type', 2).count('id as ids')

    return count[0]
  }

  * validVote(markup_id, user_id) {
    const valid = yield Database.select('id').from('votes')
      .where('markup_id', markup_id).where('user_id', user_id)

    return valid[0]
  }

  * userHasvoted(request, response) {
    const markup = yield Markup.findBy('id', request.param('id'))
    if (markup) {
      const valid = yield Database.select('id').from('votes')
        .where('markup_id', markup.id).where('user_id', request.currentUser.id)

        yield response.ok({
          vote: valid[0]
        })
    }else{
      yield response.badRequest({
        message: 'Marcação inexistente.'
      })
    }
  }

  * get(request, response) {

    const sql = yield Vote.all()

    //const votes = (yield Database.raw(sql)).shift()

    response.ok({
      message: 'foi',
      object: sql
    })
  }

}

module.exports = VoteController
