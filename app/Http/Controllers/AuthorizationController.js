'use strict'

const Database = use('Database')

class AuthorizationController {

  * validPermission(request, requiredPermissions) {
    
    const query = `
            SELECT u.id 
            FROM users u 
            LEFT JOIN profile_has_permissions php ON php.profile_id = u.profile_id
            LEFT JOIN permissions p ON p.id = php.permission_id
            WHERE u.id = ${request.currentUser.id} AND p.description 
            IN ("${requiredPermissions}")`

    const ret = (yield Database.raw(query)).shift()

    return {
      access: ret[0]
    }
  }
}

module.exports = AuthorizationController
