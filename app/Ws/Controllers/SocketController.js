'use strict'

class SocketController {

  constructor(socket, request, presence) {

    this.socket = socket
    this.request = request

    console.log('connected', this.socket.id) 
  }

  disconnected(socket) {

    console.log('disconnected socket id', socket.id)
  }

}

module.exports = SocketController
