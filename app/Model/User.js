'use strict'

const Lucid = use('Lucid')
const Hash = use('Hash')

class User extends Lucid {
  static get rules() {
    return {
      profile: 'required|integer',
      email: 'required|unique:users',
      name: 'required',
      password: 'required'
    }
  }

  static boot() {
    super.boot()

    /**
     * Hashing password before storing to the
     * database.
     */
    this.addHook('beforeCreate', function* (next) {
      this.password = yield Hash.make(this.password)
      yield next
    })

  }

  apiTokens() {
    return this.hasMany('App/Model/Token')
  }

  profile() {
    return this.belongsTo('App/Model/Profile')
  }

}

module.exports = User
