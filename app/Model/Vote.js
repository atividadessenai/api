'use strict'

const Lucid = use('Lucid')

class Vote extends Lucid {

  static get rules() {
    return {
      markup_id: 'required',
      type: 'required'
    }
  }
}

module.exports = Vote
