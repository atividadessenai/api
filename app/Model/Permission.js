'use strict'

const Lucid = use('Lucid')

class Permission extends Lucid {
  profile_has_permission() {
    return this.hasMany('App/Model/Profile_Has_Permission')
  }

  static get rules() {
    return {
      slug: 'required|integer|unique:permissions',
      description: 'required'
    }
  }

  static editRules() {
    return {
      description: 'required'
    }
  }
}

module.exports = Permission
