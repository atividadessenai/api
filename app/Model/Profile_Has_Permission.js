'use strict'

const Lucid = use('Lucid')

class Profile_Has_Permission extends Lucid {
  permission() {
    return this.belongsTo('App/Model/Permission')
  }

  profile() {
    return this.belongsTo('App/Model/Profile')
  }

  static get rules() {
    return {
      profile_id: 'required',
      permission_id: 'required'
    }
  }
}

module.exports = Profile_Has_Permission
