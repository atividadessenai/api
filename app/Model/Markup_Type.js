'use strict'

const Lucid = use('Lucid')

class Markup_Type extends Lucid {

  static get rules() {
    return {
      duration: 'required',
      description: 'required'
    }
  }

}

module.exports = Markup_Type
