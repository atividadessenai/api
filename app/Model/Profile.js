'use strict'

const Lucid = use('Lucid')

class Profile extends Lucid {
  user() {
    return this.hasOne('App/Model/User')
  }

  php() {
    return this.hasMany('App/Model/Profile_Has_Permission')
  }

  permissions() {
    return this.hasManyThrough('App/Model/Permission', 'App/Model/Profile_Has_Permission')
  }

  static get rules() {
    return {
      description: 'required'
    }
  }
}

module.exports = Profile
