'use strict'

const Lucid = use('Lucid')

class Image extends Lucid {

  markup() {
    return this.belongsTo('App/Model/Markup')
  }
}

module.exports = Image
