'use strict'

const Lucid = use('Lucid')

class Markup extends Lucid {
  static get rules() {
    return {
      markup_type_id: 'required',
      latitude: 'required',
      longitude: 'required',
      description: "required"
    }
  }

  static editRules(markupId) {
    return {
      description: "required"
    }
  }

  images() {
    return this.hasMany('App/Model/Image')
      .where('status', 1)
  }

  notification(){
      return this.hasMany('App/Model/Notification')
  }
}

module.exports = Markup
