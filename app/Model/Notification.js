'use strict'

const Lucid = use('Lucid')

class Notifications extends Lucid {

  static get rules() {
    return {
      description: 'required',
      detail: 'required'
    }
  }

  static editRules() {
    return {
      description: 'required',
      detail: 'required'
    }
  }

  profile() {
    return this.belongsTo('App/Model/Markups')
  }

}

module.exports = Notifications
