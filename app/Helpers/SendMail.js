'use strict';

const nodemailer = use('nodemailer');

class SendMail {

  * mail(user, password) {
    console.log('Startig email...')

    let transporter = nodemailer.createTransport({
      host: 'smtp.gmail.com',
      port: 587,
      secure: false,
      auth: {
        user: 'testedefesacivil@gmail.com',
        pass: 'defesacivil'
      },
      logger: false,
      debug: false // include SMTP traffic in the logs
    }, {
      // default message fields

      // sender info
      from: 'Defesa Civil De Joinville <defesacivil@org.com>'
    });

    // Message object
    let message = {
      // Comma separated list of recipients
      to: user.email,

      // Subject of the message
      subject: `Alteração de senha para o email ${user.email}`,

      // plaintext body
      text: ``,

      // HTML body
      html: `Ola ${user.name}!<p>Sua senha foi redefinida através do aplicativo da Defesa Civil de Joinville,</p>
            <p>Agora sua senha é: <b>${password}</b></p>`,
    };

    transporter.sendMail(message, (error, info) => {

      console.log('Sending email...')
      if (error) {
        console.log('Error occurred');
        console.log(error.message);
        return process.exit(1);
      }

      console.log('Message sent successfully!');

      // only needed when using pooled connections
      transporter.close();

    });
  }
}

module.exports = SendMail
