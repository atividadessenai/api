'use strict'

const Schema = use('Schema')

class UsersTableSchema extends Schema {

  up() {
    this.createIfNotExists('users', (table) => {
      table.increments('id')
      table.integer('profile_id').unsigned().references('id').inTable('profiles')
      table.string('name', 255).notNullable()
      table.string('email', 255).notNullable()
      table.string('password', 255).notNullable()
      table.integer('status', 1).notNullable()
      table.integer('receive_notification', 1).notNullable()
      table.timestamps()
    })
  }

  down() {
    this.drop('users')
  }

}

module.exports = UsersTableSchema

/*
1494887569950

1494891318038
*/
