'use strict'

const Schema = use('Schema')

class ProfilesTableSchema extends Schema {

  up () {
    this.createIfNotExists('profiles', (table) => {
      table.increments('id')
      table.string('description', 255).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('profiles')
  }

}

module.exports = ProfilesTableSchema
