'use strict'

const Schema = use('Schema')

class ImagesTableSchema extends Schema {

  up() {
    this.createIfNotExists('images', (table) => {
      table.increments('id')
      table.integer('markup_id').unsigned().references('id').inTable('markups')
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.string('path', 255).notNullable().unique()
      table.integer('status', 1).notNullable()
      table.timestamps()
    })
  }

  down() {
    this.drop('images')
  }

}

module.exports = ImagesTableSchema
