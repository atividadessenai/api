'use strict'

const Schema = use('Schema')

class NotificationsTableSchema extends Schema {

  up() {
    this.createIfNotExists('notifications', (table) => {
      table.increments('id')
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.integer('markup_id').unsigned().references('id').inTable('markups')
      table.string('description', 255).notNullable()
      table.string('detail', 255).notNullable()
      table.integer('sent', 1).notNullable()
      table.timestamps()
    })
  }

  down() {
    this.drop('notifications')
  }

}

module.exports = NotificationsTableSchema
