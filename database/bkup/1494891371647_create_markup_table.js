'use strict'

const Schema = use('Schema')

class MarkupsTableSchema extends Schema {

  up() {
    this.createIfNotExists('markups', (table) => {
      table.increments('id')
      table.integer('markup_type_id').references('id').inTable('markup_types')
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.float('latitude').notNullable()
      table.float('longitude').notNullable()
      table.string('description').notNullable()
      table.integer('status', 1).notNullable()
      table.timestamps()
    })
  }

  down() {
    this.drop('markups')
  }

}

module.exports = MarkupsTableSchema
