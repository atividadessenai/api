'use strict'

const Schema = use('Schema')

class ProfileHasPermissionsTableSchema extends Schema {

  up() {
    this.createIfNotExists('profile_has_permissions', (table) => {
      table.integer('profile_id').references('id').inTable('profiles')
      table.integer('permission_id').references('id').inTable('permissions')
      table.timestamps()
    })
  }

  down() {
    this.drop('profile_has_permissions')
  }

}

module.exports = ProfileHasPermissionsTableSchema
