'use strict'

const Schema = use('Schema')

class ProfilesTableSchema extends Schema {

  up() {
    this.table('profiles', (table) => {
      table.integer('status', 1).notNullable()
    })
  }

  down() {
    this.table('profiles', (table) => {
      // opposite of up goes here
    })
  }

}

module.exports = ProfilesTableSchema
