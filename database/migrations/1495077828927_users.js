'use strict'

const Schema = use('Schema')

class UsersTableSchema extends Schema {

  up() {
    this.create('users', (table) => {
      table.increments('id')
      table.integer('profile_id').unsigned().notNullable().references('id').inTable('profiles')
      table.string('name', 255).notNullable()
      table.string('email', 255).notNullable()
      table.string('password', 255).notNullable()
      table.integer('status', 1).notNullable()
      table.integer('receive_notification', 1).notNullable()
      table.timestamps()
    })
  }

  down() {
    this.drop('users')
  }

}

module.exports = UsersTableSchema
