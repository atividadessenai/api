'use strict'

const Schema = use('Schema')

class ProfileHasPermissionsTableSchema extends Schema {

  up() {
    this.create('profile_has_permissions', (table) => {
      table.integer('profile_id').references('id').inTable('profiles').unsigned().notNullable()
      table.integer('permission_id').references('id').inTable('permissions').unsigned().notNullable()
      table.timestamps()
    })
  }

  down() {
    this.drop('profile_has_permissions')
  }

}

module.exports = ProfileHasPermissionsTableSchema
