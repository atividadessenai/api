'use strict'

const Schema = use('Schema')

class MarkupTypesTableSchema extends Schema {

  up() {
    this.create('markup_types', (table) => {
      table.increments('id')
      table.string('description', 254).notNullable().unique()
      table.string('duration', 254).notNullable()
      table.timestamps()
    })
  }

  down() {
    this.drop('markup_types')
  }

}

module.exports = MarkupTypesTableSchema
