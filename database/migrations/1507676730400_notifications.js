'use strict'

const Schema = use('Schema')

class NotificationsTableSchema extends Schema {

  up() {
    this.table('notifications', (table) => {
      table.integer('status', 1).notNullable()
    })
  }

  down() {
    this.table('notifications', (table) => {
      // opposite of up goes here
    })
  }

}

module.exports = NotificationsTableSchema
