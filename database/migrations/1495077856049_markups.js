'use strict'

const Schema = use('Schema')

class MarkupsTableSchema extends Schema {

  up() {
    this.create('markups', (table) => {
      table.increments('id')
      table.integer('markup_type_id').unsigned().notNullable().references('id').inTable('markup_types')
      table.integer('user_id').unsigned().notNullable().references('id').inTable('users')
      table.string('latitude').notNullable()
      table.string('longitude').notNullable()
      table.string('description').notNullable()
      table.integer('status', 1).notNullable()
      table.timestamps()
    })
  }

  down() {
    this.drop('markups')
  }

}

module.exports = MarkupsTableSchema
