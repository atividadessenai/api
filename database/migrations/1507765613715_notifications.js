'use strict'

const Schema = use('Schema')

class NotificationsTableSchema extends Schema {

  up() {
    this.table('notifications', (table) => {
      table.text('detail').notNullable()
    })
  }

  down() {
    this.table('notifications', (table) => {
      // opposite of up goes here
    })
  }

}

module.exports = NotificationsTableSchema
