'use strict'

const Schema = use('Schema')

class NotificationsTableSchema extends Schema {

  up() {
    this.table('notifications', (table) => {
      table.dropColumn('detail')
    })
  }

  down() {
    this.table('notifications', (table) => {
      // opposite of up goes here
    })
  }

}

module.exports = NotificationsTableSchema
