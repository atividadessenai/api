'use strict'

const Schema = use('Schema')

class VotesTableSchema extends Schema {

  up () {
    this.create('votes', (table) => {
      table.increments('id')
      table.integer('markup_id').unsigned().notNullable().references('id').inTable('markups')
      table.integer('user_id').unsigned().notNullable().references('id').inTable('users')
      table.integer('type', 1).unsigned().notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('votes')
  }

}

module.exports = VotesTableSchema
