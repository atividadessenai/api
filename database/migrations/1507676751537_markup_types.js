'use strict'

const Schema = use('Schema')

class MarkupTypesTableSchema extends Schema {

  up() {
    this.table('markup_types', (table) => {
      table.integer('status', 1).notNullable()
    })
  }

  down() {
    this.table('markup_types', (table) => {
      // opposite of up goes here
    })
  }

}

module.exports = MarkupTypesTableSchema
