'use strict'

const Schema = use('Schema')

class PermissionsTableSchema extends Schema {

  up () {
    this.create('permissions', (table) => {
      table.increments('id')
      table.string('slug', 255).notNullable()
      table.string('description', 255).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('permissions')
  }

}

module.exports = PermissionsTableSchema
